# Fleet Meeting Summary log
## Attendees
- Cherry
- Morgana (aka Ziktofel)
- Market
- Aleyna

## Armada
- We'll get rid of `Trident Arc` as there's still no contact (#5) after Operation Rebirth (#8)

## Gitlab
- We're looking for KB authors if you don't know what to contribute, see at #1, #2, #3 or #7
- If you don't know how to contribnute, ask Ziktofel

## Officers
- Elista was demoted due to missing the events he arranged himself

## Internal
- The Cherry's action not consulted with anyone of opening outgoing dil contributions to armada. This will get get escalated to Brea (@Chuaos) (#9)
- The outgoing dil contributions were temporatily disabled till Brea (@Chuaos) decies 
- Next time actions like these shall not be done without any communication

# 16th Air Assault Brigade STO
## How to make changes?
1. Fork this repo (use the "fork" button)
2. Make your changes
3. Send a "Merge Request"  

We'll review your contribution and if it's good enough we'll accept it  
Bro tip: Check CONTRIBUTING.md for better contributions!

## Something's wrong!
Make an issue or (better) a Merge Request and fix it